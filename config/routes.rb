Rails.application.routes.draw do
  devise_for :admins
  #devise_for :admins
  get '/admin', to: 'admin/home#index'
  get '/home', to:'application#home'
  get '/show', to: 'application#show'
  get '/about', to: 'application#about'
  get '/contact', to: 'application#contact'
  get '/products', to: 'application#products'
  root 'application#home'
  namespace :admin do
    resources :categories
    resources :elements
    resources :brands
  end
  resources :elements, only:[:show,:index]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
