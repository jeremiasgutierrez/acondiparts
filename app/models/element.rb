class Element < ApplicationRecord
  has_attached_file :image, styles: { medium: "270x300!", thumb: "100x100!" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  belongs_to :category, required: false
  belongs_to :brand
  searchkick


  def search_data
      {
        title: title,
        brand: brand
      }
  end



end
