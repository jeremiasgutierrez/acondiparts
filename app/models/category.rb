class Category < ApplicationRecord
  has_many :elements
  has_many :categories
  belongs_to :category, required: false

end
