class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :home

  def home
    @elements = Element.paginate(:page => params[:page], :per_page => 16)
    @categories = Category.all.where(category_id:nil)
  end

  def products
    @found = Element.search(params['search'], fields: [:title, :brand])
  end
end
