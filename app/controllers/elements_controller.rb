class ElementsController < ApplicationController
  before_action :set_element, only: [:show]


  def show

  end

  def set_element
    @element = Element.find(params[:id])
  end

  def index
    @found = Element.search(params['search'], fields: [:title, :brand])
  end


end
