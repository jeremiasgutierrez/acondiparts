json.extract! element, :id, :title, :description, :image, :created_at, :updated_at
json.url element_url(element, format: :json)
