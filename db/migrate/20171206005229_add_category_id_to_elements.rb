class AddCategoryIdToElements < ActiveRecord::Migration[5.1]
  def change
    add_reference :elements, :category, foreign_key: true
  end
end
