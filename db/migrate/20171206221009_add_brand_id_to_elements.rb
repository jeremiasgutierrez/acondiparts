class AddBrandIdToElements < ActiveRecord::Migration[5.1]
  def change
    add_reference :elements, :brand, foreign_key: true
  end
end
