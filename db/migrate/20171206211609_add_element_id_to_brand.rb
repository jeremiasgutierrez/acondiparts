class AddElementIdToBrand < ActiveRecord::Migration[5.1]
  def change
    add_reference :brands, :element, foreign_key: true
  end
end
