class AddPriceToElements < ActiveRecord::Migration[5.1]
  def change
    add_column :elements, :price, :integer
  end
end
